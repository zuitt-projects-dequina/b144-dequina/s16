// Repetition Control Structures
/*
		Loops - lets us execute code repeatedly in a pre-set of time or maybe forever (infinite loop)
*/


/*
	WHILE LOOP
			- it takes in an expression/condition before proceeding in the evaluation of the codes.

		Syntax:
			while(expression/condition) {
				statement/s;
			}
*/

	let count = 5;

	while(count !== 0) {
		console.log(`While loop: ${count}`);

		count --;
	}


// MINI-ACTIVITY 
	console.log("// While Loop Mini-Activity //");

	let count2 = 0;

	while (count2 !== 11) {
		console.log(`While loop: ${count2}`);

		count2 ++;
	}

/*
	DO-WHILE LOOP
			- at least one code block will be executed before proceeding to the condition.

		Syntax:
			do {
				statement/s;
			} while(expression/condition)
*/

	let count3 = 5;

	do {
		console.log(`Do-while loop: ${count3}`);

		count3--;
	} while(count3 > 0);

// MINI-ACTIVITY
	console.log("// Do-while Loop Mini-Activity//");

	let count4 = 0;

	do {
		console.log(`Do-while loop: ${count4}`);

		count4 ++; 	
	} while (count4 !== 11);



/*
	FOR LOOP
			- a more flexible looping construct.

		Syntax:
			for(initialization; expression/condition; final expression){
				statement/s;
			}
*/
	//sample1
	for(let count5 = 5; count5 > 0; count5--){
		console.log(`For loop: ${count5}`)
	}

	//sample2
	//let number = Number(prompt("Give me a number:"));

	// for(let numCount = 1; numCount <= number; numCount++){
	// 	console.log(`Hello Batch 144!`)
	// }

	let myString = 'alex';
	console.log(myString.length);
	// console.log(myString[2]);

	for(let x=0; x < myString.length; x++) {
		console.log(myString[x]);
	}

/* REMEMBER for ARRAYS:

	element - represents the calues in the array
	
		elements - a l e x

	index - location of values(elements) in the array which starts at index 0


		index - 0 1 2 3 
*/
	console.log("/////////////////////////")
	let myName = "Alex";

	for(let i = 0; i < myName.length; i++) {
		if (
			myName[i].toLowerCase() == 'a' ||
			myName[i].toLowerCase() == 'e' ||
			myName[i].toLowerCase() == 'i' ||
			myName[i].toLowerCase() == 'o' ||
			myName[i].toLowerCase() == 'u'
			){
				// if the letter in the name is a vowel, it will print 3
				console.log(3)
			}
			else {
				// will print in the console if character is non-vowel
				console.log(myName[i]);
			}
		}


/////// CONTINUE AND BREAK STATEMENTS
/* 
	Continue - allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	
	Break - used to terminate the current loop once a match has been found
*/

	for(let count = 0; count <= 20; count++) {

		if(count % 2 === 0) {
			// tells the code to continue to the next iteration of the loop
			continue;
		}

		// The current value of number is printed out if the remainder is not equal to 0
		console.log("Continue and Break: " + count);

		// if the current value of count is greater than 10
		if(count > 10) {
			// Tells the code to terminate the loop even if the loop condition is still being satisfied.
			break;
		}
	}

	let name = "Alexandro";

	for(let i=0; i < name.length; i++) {
		console.log(name[i]);

		if (name[i].toLowerCase() === "a") {
			console.log("Continue to the next iteration");
			continue;
		}

		if(name[i].toLowerCase() === "d") {
			break;
		}
	}