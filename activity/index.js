// Activity on Repetition Control Structures


let num = parseInt(prompt("Please provide a number:"));
let numHolder = num;

console.log(`The number you provided is ${num}.`);


for(let count = 0; count <= numHolder; count++) {
	if (num <= 49) {
		break;
	}
	
	else if (num % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		num--;
	}

	else if (num % 5 == 0) {
		console.log(num)
		num--;
	}

	else if (((num % 10) != 0) && ((num % 5) != 0)) {
		num--;
		continue;
	}

	else if (num == 50) {
		console.log("The current number is 50. Terminating the loop.")
		break;
	}
}



let word = "supercalifragilisticexpialidocious";
console.log(word);
let noVowels = "";

for(let i = 0; i < word.length; i++) {
		if (
			word[i].toLowerCase() == 'a' ||
			word[i].toLowerCase() == 'e' ||
			word[i].toLowerCase() == 'i' ||
			word[i].toLowerCase() == 'o' ||
			word[i].toLowerCase() == 'u'
			){
				continue;
			}
			else {
				noVowels += word[i];				
			}
		}
console.log(noVowels);
